WORKING_DIR = "/fast/users/helmsauk_c/work/nb-cl-wgs/"
FASTQ_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/"
BAM_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-wgs-bam/"
FREEC_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-wgs-freec/"
SVABA_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-wgs-svaba/"
SNV_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-wgs-snv/"
BBMAP_DIR = "/fast/users/helmsauk_c/work/miniconda/envs/qc/opt/bbmap-38.58-0/"
ADAPTER_SEQUENCES = BBMAP_DIR + "resources/adapters.fa"

REFERENCE_PREINDEXED = "/fast/projects/cubit/current/static_data/precomputed/BWA/0.7.15/hg19/ucsc/hg19.fa"
SAMTOOLS_PATH = "/fast/users/helmsauk_c/work/miniconda/bin/samtools"
SVABA_BIN_PATH = "/fast/users/helmsauk_c/work/svaba/bin/"
PICARD_JAR = '/fast/users/helmsauk_c/work/Applications/picard-2.20.4/picard.jar'
TMP_DIR = "/fast/users/helmsauk_c/scratch/tmp"

# cp /fast/projects/cubit/current/static_data/precomputed/BWA/0.7.15/hg19/ucsc/hg19.fa /fast/users/helmsauk_c/scratch/hg19_bwa/hg19.fa
REFERENCE_FASTA = "/fast/users/helmsauk_c/scratch/hg19_bwa/hg19.fa"
REFERENCE_DICT = "/fast/users/helmsauk_c/scratch/hg19_bwa/hg19.dict" # file name to be computed

EXP = ["IMR-5-75", "NGP", "CHP-212", "TR-14", "LAN-1", "Kelly"]

#EXP = "NGPToy"
# prepare the chromosomes path for freec:
# cd /fast/helmsauk_c/scratch/resources/hg19_chromosomes
# wget --timestamping 'ftp://hgdownload.cse.ucsc.edu/goldenPath/hg19/chromosomes/*'
# gunzip *.fa.gz

rule all:
    input:
        # expand("{fqdir}{e}_IlluminaWGS_{r}_fastqc.html", fqdir=FASTQ_DIR, e=EXP, r=["R1", "R2"]),
        # expand("{fqdir}{e}_IlluminaWGS_{r}.trimmed_fastqc.html", fqdir=FASTQ_DIR, e=EXP, r=["R1", "R2"]),
        # FASTQ_DIR + "multiqc_report.html",
        expand(FREEC_DIR + "{e}/{e}_IlluminaWGS.hg19.rmdup.fixRG.bam_ratio.bed", e=EXP),
        expand(FREEC_DIR + "{e}/{e}_IlluminaWGS.hg19.rmdup.fixRG.bam_ratio.txt.log2.png", e=EXP),
        expand(FREEC_DIR + "{e}/{e}_IlluminaWGS.hg19.rmdup.fixRG.bam_CNVs.p.value.txt", e=EXP),
        expand(SVABA_DIR + "{e}.svaba.report.html", e=EXP),
        expand(SNV_DIR + "{e}.ann.vcf", e=EXP),
        expand(SNV_DIR + "{e}.ann.vcf.EffOnePerLine.txt", e=EXP),
        WORKING_DIR + "multiqc_report.html"

rule re_pair:
    input:
        r1 = FASTQ_DIR + "{exp}_R1.fq.gz",
        r2 = FASTQ_DIR + "{exp}_R2.fq.gz"
    output:
        r1_repaired = FASTQ_DIR + "{exp}_R1.repaired.fq.gz",
        r2_repaired = FASTQ_DIR + "{exp}_R2.repaired.fq.gz"
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        BBMAP_DIR + "repair.sh -Xmx32g in={input.r1} in2={input.r2} out={output.r1_repaired} out2={output.r2_repaired} ain=t"

rule adapter_trimming:
    input:
        r1 = FASTQ_DIR + "{exp}_R1.repaired.fq.gz",
        r2 = FASTQ_DIR + "{exp}_R2.repaired.fq.gz"
    output:
        r1_trimmed = FASTQ_DIR + "{exp}_R1.trimmed.fq.gz",
        r2_trimmed = FASTQ_DIR + "{exp}_R2.trimmed.fq.gz"
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        BBMAP_DIR + "bbduk.sh in1={input.r1} in2={input.r2} out1={output.r1_trimmed} out2={output.r2_trimmed} ktrim=r k=23 mink=11 hdist=1 ref=" + ADAPTER_SEQUENCES + " tbo"

rule fastqc:
    input:
        FASTQ_DIR + "{exp}.fq.gz"
    output:
        FASTQ_DIR + "{exp}_fastqc.html"
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        "fastqc {input}"

rule multiqc:
    input:
        expand(FASTQ_DIR + "{e}_IlluminaWGS_{r}_fastqc.html", e=EXP, r=["R1", "R2"]),
        expand(FASTQ_DIR + "{e}_IlluminaWGS_{r}.trimmed_fastqc.html", e=EXP, r=["R1", "R2"]),
        expand(BAM_DIR + "{e}_IlluminaWGS.hg19.rmdup.bam", e=EXP)
    output:
        WORKING_DIR + "multiqc_report.html"
    conda:
        WORKING_DIR + "qc.yaml"
    shell:
        "multiqc " + FASTQ_DIR + " " + BAM_DIR + " -f -o " + WORKING_DIR

rule bwa_index:
    input:
        REFERENCE_FASTA
    output:
        REFERENCE_FASTA + ".bwt"
    params:
        samtools_bin = "/fast/users/helmsauk_c/work/miniconda/bin/samtools"
    shell:
        "{params.samtools_bin} faidx {input} && bwa index {input} && touch {input}.done"

rule reference_gatk_dict:
    input:
        REFERENCE_FASTA
    output:
        REFERENCE_DICT
    conda:
        WORKING_DIR + "gatk.yaml"
    shell:
        "gatk CreateSequenceDictionary -R {input}"

rule alignment:
    input:
        ref = REFERENCE_PREINDEXED,
        # ref = REFERENCE_FASTA,
        # ref_indexing_done = REFERENCE_FASTA + ".bwt"
        r1 = FASTQ_DIR +  "{exp}_IlluminaWGS_R1.trimmed.fq.gz",
        r2 = FASTQ_DIR +  "{exp}_IlluminaWGS_R2.trimmed.fq.gz"
    output:
        bam = BAM_DIR + "{exp}_IlluminaWGS.hg19.bam"
    conda:
        WORKING_DIR + "bwa.yaml"
    params:
        threads = "10",
        samtools_bin = "/fast/users/helmsauk_c/work/miniconda/bin/samtools"
    shell:
        "bwa mem {input.ref} {input.r1} {input.r2} -t {params.threads} | {params.samtools_bin} sort -@{params.threads} -o {output.bam} - && {params.samtools_bin} index {output.bam} && {params.samtools_bin} stats {output.bam} > {output.bam}.stats.txt"

rule remove_duplicates:
    input:
        bam = BAM_DIR + "{exp}_IlluminaWGS.hg19.bam"
    output:
        bam_markdup = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup.bam",
        bam_markdup_metrics = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup-metrics.txt"
    params:
        tmp_dir = TMP_DIR,
        picard_jar = PICARD_JAR
    conda:
        WORKING_DIR + "arima-mapping.yaml"
    shell:
        "java -Xmx30G -XX:-UseGCOverheadLimit -Djava.io.tmpdir={params.tmp_dir} -jar {params.picard_jar} MarkDuplicates INPUT={input.bam} OUTPUT={output.bam_markdup} METRICS_FILE={output.bam_markdup_metrics} TMP_DIR={params.tmp_dir} REMOVE_DUPLICATES=TRUE MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 CREATE_INDEX=TRUE"

rule add_read_group:
    input:
        BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup.bam"
    output:
        bam = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam",
        bai = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam.bai"
    params:
        tmp_dir = TMP_DIR,
        picard_jar = PICARD_JAR,
        exp = "{exp}",
        samtools_bin = "/fast/users/helmsauk_c/work/miniconda/bin/samtools"
    conda:
        WORKING_DIR + "arima-mapping.yaml"
    shell:
        "java -Xmx30G -XX:-UseGCOverheadLimit -Djava.io.tmpdir={params.tmp_dir} -jar {params.picard_jar} AddOrReplaceReadGroups I={input} O={output.bam} RGID=1 RGLB=LibraryFooBar RGPL=illumina RGPU=UnitFooBar RGSM={params.exp} && {params.samtools_bin} index {output.bam}"

rule freec:
    input:
        bam = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam"
        #bam_markdup_metrics = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup-metrics.txt"
    output:
        ratios_txt_file = FREEC_DIR + "{exp}/{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam_ratio.txt",
        CNVs_file = FREEC_DIR + "{exp}/{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam_CNVs"
    params:
        config_file = WORKING_DIR + "freec-configs/{exp}.freec-config.txt",
    conda:
        WORKING_DIR + "freec.yaml"
    shell:
        "freec -conf {params.config_file}"

rule freec_bed:
    input:
        ratios_txt_file = FREEC_DIR + "{exp}/{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam_ratio.txt"
    output:
        ratios_bed_file = FREEC_DIR + "{exp}/{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam_ratio.bed"
    params:
        ploidy="2"
    conda:
        WORKING_DIR + "freec.yaml"
    shell:
        "freec2bed.pl -f {input.ratios_txt_file} -p {params.ploidy} > {output}"

rule freec_plot:
    input:
        ratios_txt_file = FREEC_DIR + "{exp}/{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam_ratio.txt"
    output:
        FREEC_DIR + "{exp}/{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam_ratio.txt.log2.png"
    params:
        ploidy="2",
        makeGraph_path = "/fast/users/helmsauk_c/work/miniconda/envs/freec/bin/makeGraph.R"
    conda:
        WORKING_DIR + "freec.yaml"
    shell:
        "cat {params.makeGraph_path} | R --slave --args {params.ploidy} {input.ratios_txt_file}"

rule freec_calculate_significance:
    input:
        ratio_txt_file = FREEC_DIR + "{exp}/{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam_ratio.txt",
        CNVs_file = FREEC_DIR + "{exp}/{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam_CNVs"
    output:
        FREEC_DIR + "{exp}/{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam_CNVs.p.value.txt"
    params:
        assess_significance_path = "/fast/users/helmsauk_c/work/miniconda/envs/freec/bin/assess_significance.R"
    conda:
        WORKING_DIR + "freec.yaml"
    shell:
        "cat {params.assess_significance_path} | R --slave --args {input.CNVs_file} {input.ratio_txt_file}"

# maybe add requirement for bam index, makes it faster I think
rule svaba:
    input:
        reference_indexed = REFERENCE_FASTA + ".done",
        bam = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam",
        bam_index = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam.bai"
        #bam_markdup_metrics = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup-metrics.txt"
    output:
        sv = SVABA_DIR + "{exp}.svaba.sv.vcf.gz",
        indel = SVABA_DIR + "{exp}.svaba.indel.vcf.gz"
    params:
        done_file = SVABA_DIR + "{exp}/done.txt",
        threads = 10,
        reference_genome_for_bwa = REFERENCE_FASTA,
        exp = SVABA_DIR + "{exp}",
        blacklist_bed = "/fast/users/helmsauk_c/work/resources/svaba-support/svaba_exclusions.bed" # from https://data.broadinstitute.org/snowman/
    shell:
        SVABA_BIN_PATH + "svaba run -z -t {input.bam} -a {params.exp} -p {params.threads} -G {params.reference_genome_for_bwa} --germline --read-tracking --blacklist {params.blacklist_bed}"

rule unzip_svaba:
    input:
        SVABA_DIR + "{files}.vcf.gz"
    output:
        SVABA_DIR + "{files}.vcf"
    shell:
        "gunzip {input}"

rule svaba_report:
    input:
        sv_unzipped = SVABA_DIR + "{exp}.svaba.sv.vcf",
        indel_unzipped = SVABA_DIR + "{exp}.svaba.indel.vcf"
    output:
        report = SVABA_DIR + "{exp}.svaba.report.html"
    params:
        exp = "{exp}",
        svaba_prefix = SVABA_DIR + "{exp}.svaba",
        make_sv_report_script = WORKING_DIR + "makeSVReport.R",
        sv_report_template = WORKING_DIR + "SVReport.Rmd"
    conda:
        WORKING_DIR + "r-wgs2.yaml"
    shell:
        "Rscript {params.make_sv_report_script} {params.sv_report_template} {params.svaba_prefix}.indel.vcf {params.svaba_prefix}.indelfromsv.vcf {params.svaba_prefix}.sv.vcf {params.exp} {output}"

rule snv:
    input:
        reference_bwt = REFERENCE_FASTA + ".bwt", # make sure there is a faidx
        reference_dict = REFERENCE_DICT, # make sure there is a reference dict
        bam = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam",  # it needs the sample name in the read group field of bam, otherwise error.
        bam_bai = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup.fixRG.bam.bai" # also needs to be indexed
        #bam_markdup_metrics = BAM_DIR + "{exp}_IlluminaWGS.hg19.rmdup-metrics.txt"
    output:
        SNV_DIR + "{exp}.vcf"
    params:
        tmp_dir = TMP_DIR,
        reference = REFERENCE_FASTA
    conda:
        WORKING_DIR + "gatk.yaml"
    shell:
        "gatk --java-options '-Xmx4g' HaplotypeCaller -R {params.reference} -I {input.bam} -O {output} -bamout {output}.bamout.bam"

rule snpeff:
    input:
        SNV_DIR + "{exp}.vcf"
    output:
        SNV_DIR + "{exp}.ann.vcf"
    params:
        unzipped_input = SNV_DIR + "{exp}.vcf",
        snpEff_path = "/fast/users/helmsauk_c/work/miniconda/envs/snpeff/share/snpeff-4.3.1t-2/"
    conda:
        WORKING_DIR + "snpeff.yaml"
    shell:
        "java -Xmx4G -jar {params.snpEff_path}snpEff.jar -c {params.snpEff_path}snpEff.config hg19 {params.unzipped_input} > {output}"

rule parse_snpeff:
    input:
        SNV_DIR + "{exp}.ann.vcf"
    output:
        SNV_DIR + "{exp}.ann.vcf.EffOnePerLine.txt"
    params:
        snpEff_path = "/fast/users/helmsauk_c/work/Applications/snpEff/"
    conda:
        WORKING_DIR + "snpeff.yaml"
    shell:
        "cat {input} | {params.snpEff_path}scripts/vcfEffOnePerLine.pl | java -jar {params.snpEff_path}SnpSift.jar extractFields - CHROM POS REF ALT ID FILTER AF AC DP MQ 'ANN[*].GENE' 'ANN[*].EFFECT' 'ANN[*].IMPACT' 'ANN[*].HGVS_C' > {output}"


#"cat {input} | {params.snpEff_path}scripts/vcfEffOnePerLine.pl | java -jar {params.snpEff_path}SnpSift.jar extractFields - CHROM POS REF ALT ID FILTER AF AC DP MQ 'ANN[*].GENE:' 'ANN[*].EFFECT:' 'ANN[*].IMPACT:' 'ANN[*].HGVS_C' > {output}"

# conda create -n vep
# conda activate vep
# conda install -c bioconda ensembl-vep
# conda install -c bioconda perl-module-build
# vep_install -s human -y hg19
# add snp ID or 1000Genomes ID
# java -jar SnpSift.jar annotate dbSnp132.vcf variants.vcf > variants_annotated.vcf
