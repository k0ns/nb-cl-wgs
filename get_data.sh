#!/bin/bash

# IMR 5 75
cp /fast/projects/Schulte_NB/work/Henssen_CellLine_WGS/Clean/IMR5-75-gDNA-Anton-Henssen/FCHKJHYALXX_L7_8521705001781_1.fq.gz \
  /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/IMR-5-75_IlluminaWGS_R1.fq.gz
cp /fast/projects/Schulte_NB/work/Henssen_CellLine_WGS/Clean/IMR5-75-gDNA-Anton-Henssen/FCHKJHYALXX_L7_8521705001781_2.fq.gz \
  /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/IMR-5-75_IlluminaWGS_R2.fq.gz

# Kelly
cat \
  /fast/projects/Schulte_NB/work/Henssen_CellLine_WGS/Clean/Kelly-gDNA-Anton-Henssen/FCHKGLLALXX_L8_8521705001779_1.fq.gz \
  /fast/projects/Schulte_NB/work/Henssen_CellLine_WGS/Clean/Kelly-gDNA-Anton-Henssen/FCHKHVMALXX_L1_8521705001779_1.fq.gz \
  > /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/Kelly_IlluminaWGS_R1.fq.gz

cat \
  /fast/projects/Schulte_NB/work/Henssen_CellLine_WGS/Clean/Kelly-gDNA-Anton-Henssen/FCHKGLLALXX_L8_8521705001779_2.fq.gz \
  /fast/projects/Schulte_NB/work/Henssen_CellLine_WGS/Clean/Kelly-gDNA-Anton-Henssen/FCHKHVMALXX_L1_8521705001779_2.fq.gz \
  > /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/Kelly_IlluminaWGS_R2.fq.gz

# LAN1
cat \
  /fast/projects/Schulte_NB/work/Henssen_CellLine_WGS/Clean/LAN1-gDNA-Anton-Henssen/FCHKHVMALXX_L1_8521705001780_1.fq.gz \
  /fast/projects/Schulte_NB/work/Henssen_CellLine_WGS/Clean/LAN1-gDNA-Anton-Henssen/FCHKJHYALXX_L8_8521705001780_1.fq.gz \
  > /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/LAN-1_IlluminaWGS_R1.fq.gz

cat \
  /fast/projects/Schulte_NB/work/Henssen_CellLine_WGS/Clean/LAN1-gDNA-Anton-Henssen/FCHKHVMALXX_L1_8521705001780_2.fq.gz \
  /fast/projects/Schulte_NB/work/Henssen_CellLine_WGS/Clean/LAN1-gDNA-Anton-Henssen/FCHKJHYALXX_L8_8521705001780_2.fq.gz \
  > /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/LAN-1_IlluminaWGS_R2.fq.gz

# NGP
cp /fast/users/helmsauk_c/scratch/NGP_IlluminaWGS_R1.fq.gz /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/NGP_IlluminaWGS_R1.fq.gz
cp /fast/users/helmsauk_c/scratch/NGP_IlluminaWGS_R2.fq.gz /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/NGP_IlluminaWGS_R2.fq.gz

# CHP-212
cp /fast/projects/NB_CircleSeq/scratch/TrimmedMergedPolyGRem/AH_KH_CHP212_WGS_R1.trimmed.polygrem.fastq.gz \
  /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/CHP-212_IlluminaWGS_R1.fq.gz
cp /fast/projects/NB_CircleSeq/scratch/TrimmedMergedPolyGRem/AH_KH_CHP212_WGS_R2.trimmed.polygrem.fastq.gz \
  /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/CHP-212_IlluminaWGS_R2.fq.gz

# TR14
cp /fast/projects/NB_CircleSeq/scratch/TrimmedMergedPolyGRem/AH_KH_TR14_WGS_R1.trimmed.polygrem.fastq.gz \
  /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/TR-14_IlluminaWGS_R1.fq.gz
cp /fast/projects/NB_CircleSeq/scratch/TrimmedMergedPolyGRem/AH_KH_TR14_WGS_R2.trimmed.polygrem.fastq.gz \
  /fast/users/helmsauk_c/scratch/nb-cl-wgs-fastq/TR-14_IlluminaWGS_R2.fq.gz
